#include <PxMatrix.h>
#include <Fonts/FreeSansBoldOblique9pt7b.h>
#include <Fonts/FreeSansBoldOblique12pt7b.h>
#include <Fonts/FreeSans9pt7b.h>

#include "logoPlane.h"
#include "time.h"


#define matrix_width 128
#define matrix_height 32

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;
// Pins for LED MATRIX
#define P_LAT 22
#define P_A 19
#define P_B 23
#define P_C 18
#define P_D 5
#define P_OE 33
PxMATRIX display(matrix_width,matrix_height,P_LAT, P_OE,P_A,P_B,P_C,P_D);

// Some standard colors
uint16_t myRED = display.color565(255, 0, 0);
uint16_t myGREEN = display.color565(0, 255, 0);
uint16_t myBLUE = display.color565(0, 0, 255);
uint16_t myWHITE = display.color565(255, 255, 255);
uint16_t myYELLOW = display.color565(255, 255, 0);
uint16_t myCYAN = display.color565(0, 255, 255);
uint16_t myMAGENTA = display.color565(255, 0, 255);
uint16_t myBLACK = display.color565(0, 0, 0);

uint16_t myCOLORS[8] = {myRED, myGREEN, myBLUE, myWHITE, myYELLOW, myCYAN, myMAGENTA, myBLACK};
// ISR for display refresh

void IRAM_ATTR display_updater(){
  // Increment the counter and set the time of ISR
  portENTER_CRITICAL_ISR(&timerMux);
  display.display(50);
  portEXIT_CRITICAL_ISR(&timerMux);
}

void display_update_enable(bool is_enable)
{
  if (is_enable)
  {
    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &display_updater, true);
    timerAlarmWrite(timer, 4000, true);
    timerAlarmEnable(timer);
  }
  else
  {
    timerDetachInterrupt(timer);
    timerAlarmDisable(timer);
  }
}

void Chord(int r, float rot)
{
  int nodes = 6;
  float x[nodes];
  float y[nodes];
  for (int i=0; i<nodes; i++)
  {
    float a = rot + (PI*2*i/nodes);
    x[i] = 63+3 + cos(a)*r;
    y[i] = 16 + sin(a)*r;
  }

  display.fillScreen(display.color565(0, 0,0));
  for (int i=0; i<(nodes-1); i++)
    for (int j=i+1; j<nodes; j++)
      display.drawLine(x[i],y[i], x[j],y[j], display.color565(0, 255,0));
}
void rotationEffect(){
  float rot;
  float rotationSpeed = PI/15;
  for (int r=1; r<44; r+=3) {
    Chord(r, rot+=rotationSpeed);
    delay(50);
  }
  for (int r=1; r<44; r+=3) {
    Chord(44-r, rot-=rotationSpeed);
    delay(30);
  }
}
void scroll_text(uint8_t ypos, unsigned long scroll_delay, String text, uint8_t colorR, uint8_t colorG, uint8_t colorB)
{
    uint16_t text_length = text.length();
    display.setTextWrap(false);  // we don't wrap text so it scrolls nicely
    display.setTextSize(1);
    display.setRotation(0);
    display.setTextColor(display.color565(colorR,colorG,colorB));

    // Asuming 5 pixel average character width
    for (int xpos=matrix_width; xpos>-(matrix_width+text_length*5); xpos--)
    {
      display.setTextColor(display.color565(colorR,colorG,colorB));
      display.clearDisplay();
      display.setCursor(xpos,ypos);
      display.println(text);
      delay(scroll_delay);
      yield();

      // This might smooth the transition a bit if we go slow
      // display.setTextColor(display.color565(colorR/4,colorG/4,colorB/4));
      // display.setCursor(xpos-1,ypos);
      // display.println(text);

      delay(scroll_delay/5);
      yield();

    }
}
void displayStartUp(){
  display.clearDisplay();
  rotationEffect();
  display.setCursor(0,0);
  display.setTextColor(myBLUE);
  display.setFont(&FreeSans9pt7b);
  scroll_text(20,15,"Nguyen Le Hoai Tram",255,0,0);
  rotationEffect();
  scroll_text(20,15,"Flight Information Display Screen ",0,0,255);
  display.clearDisplay();
  display.setTextColor(myGREEN);
  display.setFont();
  display.setCursor(0,10);
  display.print("Connecting Wifi...");
  delay(2000);
}
void displayResultConnection(bool blynkConnected){
  if (blynkConnected){
    display.clearDisplay();
    display.setTextColor(myGREEN);
    display.setCursor(0,10);
    display.print("Connected to server");
  }
  else {
    display.clearDisplay();
    display.setTextColor(myRED);
    display.setCursor(0,10);
    display.print("Connection failed...");
  }
  delay(3000);
}
void showlocationIcon(){
  display.clearDisplay();
  int imageHeight = 32;
  int imageWidth = 64;
  int counter = 0;
  for (int yy = 0; yy < imageHeight; yy++)
        {
            for (int xx = 0; xx < imageWidth; xx++)
            {

              display.drawPixel(xx, yy, locationIcon[counter]);
              counter++;
            }
         }
}
void showFlight(int airline, String flightCode,  int timeFlight, int airPort){
        display.clearDisplay();
        int imageHeight = 32;
        int imageWidth = 128;
        int counter = 0;
        for (int yy = 0; yy < imageHeight; yy++)
        {
            for (int xx = 0; xx < imageWidth; xx++)
            {
              if (airline == 1)
                display.drawPixel(xx, yy, vietNamAirline[counter]); 
              else if (airline == 2) {
                display.drawPixel(xx, yy, vietjetAir[counter]);
              }
              else if (airline == 3){
                display.drawPixel(xx, yy, jetstar[counter]);
                }
              counter++;
            }
        }
        delay(2000);
        
        display.clearDisplay();
        counter = 0;
        for (int yy = 0; yy < imageHeight; yy++)
        {
            for (int xx = 0; xx < imageWidth; xx++)
            {
              display.drawPixel(xx, yy, locationIcon[counter]);
              counter++;
            }
        }
        
        display.setTextColor(myRED);
        display.setTextSize(2);
        display.setCursor(28,0);
        display.print(flightCode);
        
        display.setTextColor(myYELLOW);
        display.setCursor(18,18);
        int hh = timeFlight/(60*60);
        int mm = (timeFlight - hh*60*60)/(60);
        
        String timeShow = "";
        if (hh < 10)
          timeShow += "0" + (String)hh+":";
        else {
          timeShow += (String)hh+":";
        }
        if (mm < 10)
          timeShow += "0" + (String)mm;
        else {
          timeShow += (String)mm;
        }
        display.print(timeShow);
        display.setTextColor(myMAGENTA);
        display.setCursor(93,18);
        
        if (airPort == 1){
            display.print("HAN");
        }
        else if (airPort == 2) {
          display.print("DAD");
        }
        else if (airPort == 3){
          display.print("VII");
        }
        display.setTextSize(1);
}
void showAds(int airline,String msg){
  display.clearDisplay();
  int imageHeight = 32;
  int imageWidth = 128;
  int counter = 0;
  for (int yy = 0; yy < imageHeight; yy++)
  {
            for (int xx = 0; xx < imageWidth; xx++)
            {
              if (airline == 1)
                display.drawPixel(xx, yy, vietNamAirline[counter]); 
              else if (airline == 2) {
                display.drawPixel(xx, yy, vietjetAir[counter]);
              }
              else if (airline == 3){
                display.drawPixel(xx, yy, jetstar[counter]);
                }
              counter++;
            }
    }
    delay(2000);
    display.clearDisplay();
    display.setFont(&FreeSans9pt7b);
    scroll_text(20,15,msg,255,255,255);
    display.setFont();
}
void showSmartConfigMode(){
    display.clearDisplay();
    display.setTextColor(myGREEN);
    display.setCursor(0,0);
    display.print("Smart Config");
    display.setCursor(0,16);
    display.print("Waiting...");
}
