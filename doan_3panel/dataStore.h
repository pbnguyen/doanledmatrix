#include <ArduinoJson.h> // Thêm thư viện ArduinoJson
#include "FS.h"          //Thư viện giao tiếp SPIFFS - sPI Flash File System
#include "SPIFFS.h"

StaticJsonDocument<1024> doc;   //Biến chứa dữ liệu JSON

#define FORMAT_SPIFFS_IF_FAILED true    

void SPIFFSInit(){
    //Khởi động SPIFFS, format lại phân vùng nếu bị lỗi
    if(!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)){
        Serial.println("SPIFFS Mount Failed");
        return;
    }
}

void loadingData(){
    Serial.println("Reading file");
    //Khai báo file tên data.txt
    File file = SPIFFS.open("/data.txt");
    //Kết thúc nếu mở file bị lỗi
    if(!file){
        Serial.println("- failed to open file for writing");
        return;
    }
    //Deserialize dữ liệu json vào biến doc
    DeserializationError error = deserializeJson(doc, file);
    if (error){ 
      Serial.println("Failed to read file");
      return;
    }
    Serial.println("Data: ");
    serializeJsonPretty(doc, Serial);
}

void saveData(){

    serializeJsonPretty(doc, Serial);
    
    Serial.println("Writing file");
    
    File file = SPIFFS.open("/data.txt", FILE_WRITE);
    if(!file){
        Serial.println("- failed to open file for writing");
        return;
    }
    // Serialize JSON to file
    if (serializeJson(doc, file) == 0) {
      Serial.println("Failed to write to file");
      return;
    }
    Serial.println("Success to write to file");
    // Close the file
    file.close();
}
