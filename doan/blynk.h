
#include "dataStore.h"
#include "showLedMatrix.h"
#include "time.h"

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 60*60*7;
const int   daylightOffset_sec = 0;

WidgetLCD lcdMode(V13);
WidgetLCD lcdData(V14);

bool needSave = false;
bool hasRevTime = false;
int eventShowCode = 0;

const char * auth = "JcrPSRhJBSd2cYBKhqggpJ_ecrieIkhS";

void printLocalTime(){
  static uint32_t lastPrint = 0;
  if ( (millis() - lastPrint) > 1000){
    lastPrint = millis();
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
  Serial.print("Day of week: ");
  Serial.println(&timeinfo, "%A");
  Serial.print("Month: ");
  Serial.println(&timeinfo, "%B");
  Serial.print("Day of Month: ");
  Serial.println(&timeinfo, "%d");
  Serial.print("Year: ");
  Serial.println(&timeinfo, "%Y");
  Serial.print("Hour: ");
  Serial.println(&timeinfo, "%H");
  Serial.print("Hour (12 hour format): ");
  Serial.println(&timeinfo, "%I");
  Serial.print("Minute: ");
  Serial.println(&timeinfo, "%M");
  Serial.print("Second: ");
  Serial.println(&timeinfo, "%S");

  Serial.println("Time variables");
  char timeHour[3];
  strftime(timeHour,3, "%H", &timeinfo);
  Serial.println(timeHour);
  char timeWeekDay[10];
  strftime(timeWeekDay,10, "%A", &timeinfo);
  Serial.println(timeWeekDay);
  Serial.println();
  }
}

void blynkInit(){
  Blynk.config(auth); // Thiết lập auth ky kết nối tới Blynk
  WiFi.mode(WIFI_STA); //Thiết lập Wifi ở chế độ Station, ESP32 sẽ kết nối tới router
  String ssid = getSSID();  // Lấy tên wifi mà ESP32 đang lưu trữ từ lần kết nối trước
  String pass = getPass(); // Lấy mật khẩu wifi mà ESP32 đang lưu trữ từ lần kết nối trước
  WiFi.begin(ssid.c_str(),pass.c_str()); // Kết nối Wifi
  //Chờ 5s nếu như không kết nối thì thoát khỏi vòng lặp chờ
  int counterTimeOut = 0; 
  while (WiFi.status() != WL_CONNECTED && counterTimeOut < 5 ) {
      delay(1000);
      Serial.print(".");
      counterTimeOut++;
  }
  Serial.println();
  //Kiểm tra có được kết nối hay không, nếu có kết nối tới Blynk
  if (WiFi.status() == WL_CONNECTED){
      Serial.println("Wifi Connected");
      bool result = Blynk.connect(5000);
      if (!result)
        Serial.println("Not connect");
      else {
        Serial.println("Connected");
        hasRevTime = true;
      }
  }
  else {
      Serial.println("Wifi Not Connected");
  }
  // Thiết lập thời gian thực cho ESP32 ở múi giờ Việt Nam: GT +7
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  printLocalTime();
}

void showLcdMode(){
    String ssid = getSSID();
    lcdMode.clear();
    lcdMode.print(0, 0, ssid);
    lcdMode.print(0, 1, "Mode: ");
    if ( doc["mode"] == 1){
      lcdMode.print(6, 1, "Auto");
    }
    else {
      lcdMode.print(6, 1, "Manual");
    }
}
void showLCDEvent(int eventCode){
    if (eventCode < 4){
      lcdData.clear();
      lcdData.print(0, 0,"Display Data");
      lcdData.print(3, 1,"Flight");
      lcdData.print(10,1, eventCode);
    }
    else {
      lcdData.clear();
      lcdData.print(0, 0,"Display Data");
      lcdData.print(3, 1,"Ads");
      lcdData.print(7,1, eventCode-3);
    }
}
void reconnectWifi(){
  static uint32_t lastReconnect = 0;
  if ((uint32_t)(millis() - lastReconnect) > 30000){
    Serial.println("Reconnecting the Wifi");
    display_update_enable(false);
    WiFi.disconnect();
    String ssid = getSSID();
    String pass = getPass();
    WiFi.begin(ssid.c_str(),pass.c_str());
        int counterTimeOut = 0;
        while (WiFi.status() != WL_CONNECTED && counterTimeOut < 5 ) {
            delay(1000);
            Serial.print(".");
            counterTimeOut++;
        }
        Serial.println();
        if (WiFi.status() == WL_CONNECTED){
            Serial.println("Wifi Connected");
            bool result = Blynk.connect(5000);
            if (!result)
              Serial.println("Not connect");
            else {
              Serial.println("Connected");
              hasRevTime = true;
            }
        }
        else {
            WiFi.disconnect();
            Serial.println("Wifi Not Connected");
        }
   display_update_enable(true);
   lastReconnect = millis();
  }
}
void reconnectBlynk(){
    if (WiFi.status() == WL_CONNECTED){
        bool result = Blynk.connect(10000);
        if (!result)
          Serial.println("Not connect");
        else {
          hasRevTime = true;
          Serial.println("Connected");
        } 
    }
    else {
        reconnectWifi();
      }
}
BLYNK_CONNECTED() {
  //get data stored in virtual pin from server
  Blynk.syncAll();
}
//V0 -> V3: Flight 1 Infor 
BLYNK_WRITE(V0) {
  int data = param.asInt();
  if (data >=1 && data <= 3){
      if (doc["flight"][0]["airline"] != data){
        doc["flight"][0]["airline"]  = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V1) {
  int data = param.asInt();
  if (data>=1 && data <= 3){
      if (doc["flight"][0]["airport"] != data){
        doc["flight"][0]["airport"] = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V2) {
  int data = param.asInt();
  if (doc["flight"][0]["flightTime"] != data){
    doc["flight"][0]["flightTime"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V3) {
  String data = param.asStr();
  if (doc["flight"][0]["flightNumber"] != data){
        doc["flight"][0]["flightNumber"] = data;
        needSave = true;
  }
}
//V4 -> V7: Flight 2 Infor 
BLYNK_WRITE(V4) {
  int data = param.asInt();
  if (data >=1 && data <= 3){
      if (doc["flight"][1]["airline"] != data){
        doc["flight"][1]["airline"] = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V5) {
  int data = param.asInt();
  if (data>=1 && data <= 3){
      if (doc["flight"][1]["airport"] != data){
        doc["flight"][1]["airport"] = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V6) {
  int data = param.asInt();
  if (doc["flight"][1]["flightTime"] != data){
    doc["flight"][1]["flightTime"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V7) {
  String data = param.asStr();
  if (doc["flight"][1]["flightNumber"] != data){
        doc["flight"][1]["flightNumber"] = data;
        needSave = true;
  }
}
//V8 -> V9: Ads 1
BLYNK_WRITE(V8) {
  int data = param.asInt();
  if (doc["ads"][0]["airline"] != data){
    doc["ads"][0]["airline"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V9) {
  String data = param.asStr();
  if (doc["ads"][0]["msg"] != data){
        doc["ads"][0]["msg"] = data;
        needSave = true;
  }
}
//V10 -> V11: Ads 2
BLYNK_WRITE(V10) {
  int data = param.asInt();
  if (doc["ads"][1]["airline"] != data){
    doc["ads"][1]["airline"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V11) {
  String data = param.asStr();
  if (doc["ads"][1]["msg"] != data){
        doc["ads"][1]["msg"] = data;
        needSave = true;
  }
}
//Mode
BLYNK_WRITE(V12) {
  int data = param.asInt();
  if (doc["mode"] != data){
        lcdData.clear();
        doc["mode"] = data;
        needSave = true;
  }
}
//Show now !!
BLYNK_WRITE(V15) {
  int data = param.asInt();
  if (doc["manualDataShow"] != data){
      doc["manualDataShow"]= data;
      needSave = true;
  }
  if (doc["mode"]  == 2){
    if (data < 4){
      lcdData.clear();
      lcdData.print(0, 0,"Show fight");
      lcdData.print(11,0, data);
      lcdData.print(0, 1,"Waiting");
    }
    else {
      lcdData.clear();
      lcdData.print(0, 0,"Show ads");
      lcdData.print(9,0, data-3);
      lcdData.print(0, 1,"Waiting");
    }
  }
  else {
      lcdData.clear();
      lcdData.print(0, 0, "Please change   manual mode !!");
  }
}

//V16 -> V19: Flight 3 Infor 
BLYNK_WRITE(V16) {
  int data = param.asInt();
  if (data >=1 && data <= 3){
      if (doc["flight"][2]["airline"] != data){
        doc["flight"][2]["airline"] = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V17) {
  int data = param.asInt();
  if (data>=1 && data <= 3){
      if (doc["flight"][2]["airport"] != data){
        doc["flight"][2]["airport"] = data;
        needSave = true;
      }
  }
}
BLYNK_WRITE(V18) {
  int data = param.asInt();
  if (doc["flight"][2]["flightTime"] != data){
    doc["flight"][2]["flightTime"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V19) {
  String data = param.asStr();
  if (doc["flight"][2]["flightNumber"] != data){
        doc["flight"][2]["flightNumber"] = data;
        needSave = true;
  }
}
//V20 -> V21: Ads 3
BLYNK_WRITE(V20) {
  int data = param.asInt();
  if (doc["ads"][2]["airline"] != data){
    doc["ads"][2]["airline"] = data;
    needSave = true;
  }
}
BLYNK_WRITE(V21) {
  String data = param.asStr();
  if (doc["ads"][2]["msg"] != data){
        doc["ads"][2]["msg"] = data;
        needSave = true;
  }
}

void runRealTimeMode(bool needUpdate){
  static int manualShowCode = 0;
  static int showAdsCode = 0;
  static uint32_t showTime = 0;
  //Auto Mode
  if (doc["mode"] == 1){
    struct tm timeinfo;
    if(!getLocalTime(&timeinfo)){
      Serial.println("Failed to obtain time");
      return;
    }
    int hh = timeinfo.tm_hour;
    int mm = timeinfo.tm_min;
    uint32_t totalSec = hh*60*60 + mm*60;
    JsonArray arrayFlight = doc["flight"];
    int sizeFlight = arrayFlight.size();
    for (int i = 0; i < sizeFlight; i++){
        if ( (arrayFlight[i]["flightTime"] > totalSec - 300) && (arrayFlight[i]["flightTime"] < totalSec + 300 )){
            if ((uint32_t) (millis() - showTime) > 10000){
                showTime = millis();
                Serial.println("Show Flight");
                showLCDEvent(i+1);
                showFlight(arrayFlight[i]["airline"],arrayFlight[i]["flightNumber"],arrayFlight[i]["flightTime"],arrayFlight[i]["airport"]);
                return;
            }
        }
    }
    JsonArray arrayAds = doc["ads"];
    int sizeAds    = arrayAds.size();
    if ((uint32_t) (millis() - showTime) > 10000){
      showTime = millis();
      Serial.printf("Auto Mode, Show Ads: %d\r\n",showAdsCode);
      showLCDEvent(showAdsCode+4);
      showAds(arrayAds[showAdsCode]["airline"],arrayAds[showAdsCode]["msg"]);
      showAdsCode++;
    }
    if (showAdsCode >= sizeAds)
      showAdsCode = 0;
  }
  //Manual Mode
  else {
     //Manual Show Flight
     if (doc["manualDataShow"]      == 1){
          if (manualShowCode != 1 || needUpdate){
            Serial.println("Show Flight 1");
            manualShowCode = 1;
            showLCDEvent(1);
            showFlight(doc["flight"][0]["airline"],doc["flight"][0]["flightNumber"],doc["flight"][0]["flightTime"],doc["flight"][0]["airport"]);
          }
     }
     else if (doc["manualDataShow"] == 2){
      if (manualShowCode != 2 || needUpdate){
          manualShowCode = 2;
          Serial.println("Show Flight 2");
          showLCDEvent(2);
          showFlight(doc["flight"][1]["airline"],doc["flight"][1]["flightNumber"],doc["flight"][1]["flightTime"],doc["flight"][1]["airport"]);
      }
     }
     else if (doc["manualDataShow"] == 3){
      if (manualShowCode != 3 || needUpdate ){
        manualShowCode = 3;
        showLCDEvent(3);
        Serial.println("Show Flight 3");
        showFlight(doc["flight"][2]["airline"],doc["flight"][2]["flightNumber"],doc["flight"][2]["flightTime"],doc["flight"][2]["airport"]);
      }
     }
     //Manual Show Ads
     else if (doc["manualDataShow"] == 4){
        Serial.println("Show Ads 1");
        showLCDEvent(4);
        showAds(doc["ads"][0]["airline"],doc["ads"][0]["msg"]);
     }
     else if (doc["manualDataShow"] == 5){
        Serial.println("Show Ads 2");
        showLCDEvent(5);
        showAds(doc["ads"][0]["airline"],doc["ads"][1]["msg"]);
     }
     else if (doc["manualDataShow"] == 6){
        Serial.println("Show Ads 3");
        showLCDEvent(6);
        showAds(doc["ads"][0]["airline"],doc["ads"][2]["msg"]);
        
     }
  }
}

void runOfflineMode(){
  static int showCode = 0;
  static uint32_t showTime = 0;
  if ((uint32_t) (millis() - showTime) > 10000){
    showTime = millis();
    JsonArray arrayFlight = doc["flight"];
    int sizeFlight = arrayFlight.size();
    JsonArray arrayAds = doc["ads"];
    int sizeAds    = arrayAds.size();
    if (showCode < sizeFlight){
      Serial.printf("Show flight: %d \r\n",showCode);
      showFlight(doc["flight"][showCode]["airline"],doc["flight"][showCode]["flightNumber"],doc["flight"][showCode]["flightTime"],doc["flight"][showCode]["airport"]);
    }
    else {
      Serial.printf("Show flight: %d \r\n",showCode - sizeFlight);
      showAds(arrayAds[showCode - sizeFlight]["airline"],arrayAds[showCode - sizeFlight]["msg"]);
    }
    showCode++;
    if (showCode >= sizeFlight + sizeAds){
        showCode = 0;
    }
  }
}
