
#include <esp_wifi.h>
#define PIN_BUTTON 32
#define TIME_OUT_SMART_CONFIG 60000
#define TIME_OUT_NO_WIFI 20000



String getSSID(){
    wifi_config_t conf;
    esp_wifi_get_config(WIFI_IF_STA, &conf);
    String ssid = String(reinterpret_cast<const char*>(conf.sta.ssid));
    return ssid;
  }
String getPass(){
    wifi_config_t conf;
    esp_wifi_get_config(WIFI_IF_STA, &conf);
    String password = String(reinterpret_cast<const char*>(conf.sta.password));
    return password;
}
void gpioInit(){
    pinMode(PIN_BUTTON, INPUT_PULLUP);
}
bool longPress(){
    static unsigned long  lastPress = 0;
    if ( (unsigned long) (millis() - lastPress) > 3000 && digitalRead(PIN_BUTTON) == 0) {
        lastPress = millis();
        return true;
    } else if (digitalRead(PIN_BUTTON) == 1) {
        lastPress = millis();
    }
    return false;
}
void enterSmartConfig() {
      Serial.println("Enter Smart Conffig");
     
      WiFi.disconnect(true);
      delay(1000);
      if(!WiFi.beginSmartConfig()){
        Serial.println("Smart Config Fail");
        ESP.restart();
        return;
      }
      int counter = 0;
      while (!WiFi.smartConfigDone()) {
        delay(500);
        counter++;
        Serial.print(".");
        if (counter > 120)
          return;
      }
      Serial.println("");
      Serial.println("SmartConfig received.");
      counter = 0;
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        counter++;
        if (counter > 10){
          WiFi.stopSmartConfig();
          return;
        }
      }

      Serial.println("WiFi Connected.");
    WiFi.stopSmartConfig();
}
