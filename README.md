# doanledmatrix

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

#include "smartconfig.h"
#include "blynk.h"

#define BLYNK_PRINT Serial

void setup() {
  Serial.begin(115200);
  delay(1000);
  gpioInit();
  display.begin(16);
  display.clearDisplay();
  display_update_enable(true);
  displayStartUp();
  display_update_enable(false);
  blynkInit();
  SPIFFSInit();
  loadingData();
  display_update_enable(true);
  if (Blynk.connected())
    displayResultConnection(true);
  else
    displayResultConnection(false);
}

void loop() {
  if(longPress()){
      showSmartConfigMode();
      delay(5000);
      display_update_enable(false);
      enterSmartConfig();
      display_update_enable(true);
  }
  else {
      if (hasRevTime) {
         runRealTimeMode(needSave);
      }
      else {
        runOfflineMode();
      }
    needSave = false;
    if (Blynk.connected()) {
      Blynk.run();
      if (needSave){
         display_update_enable(false);
         saveData();
         display_update_enable(true);
         showLcdMode();
      }
    }
    else {
       reconnectBlynk();
    }
   }
}
